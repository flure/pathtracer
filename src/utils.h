#ifndef _UTILS_H_
#define _UTILS_H_

#include <cstdlib>
// #include "vec2.h"
// #include "vec3.h"
#include "vml.h"

#define clamp(x, a, b)	(x) < (a) ? (a) : (x) > b ? (b) : (x)

vml::vec3 cosine_weighted_hemisphere(const vml::vec3& up);
vml::vec3 random_vector_in_hemisphere(const vml::vec3& up);
vml::vec2 concentric_sample_disk();


template<typename T>
T rand01()
{
	return std::rand() / static_cast<T>(RAND_MAX);
}

template<typename T>
T rand11()
{
	return T(2.0) * rand01<T>() - T(1.0);
}

float sfrand();

#endif // _UTILS_H_