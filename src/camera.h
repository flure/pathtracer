#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "vml.h"
#include "ray.h"

class Camera
{
public:
	Camera()
		: m_origin(), m_direction(0.0, 0.0, -1.0), m_fovy(36.5) {}

	Camera(double fovy)
		: m_origin(), m_direction(0.0, 0.0, -1.0), m_fovy(fovy) {}

	Camera(const vml::vec3& origin, const vml::vec3& direction, double fovy)
		: m_origin(origin), m_direction(direction), m_fovy(fovy) {}

	Camera(const Camera& other)
		: m_origin(other.m_origin), m_direction(other.m_direction), m_fovy(other.m_fovy) {}

	Camera& operator=(const Camera& other)
	{
		m_origin = other.m_origin;
		m_direction = other.m_direction;
		m_fovy = other.m_fovy;
		return *this;
	}

	Ray create_ray(const vml::vec2& v) const;

	vml::vec3 get_origin() const { return m_origin; }
	vml::vec3 get_direction() const { return m_direction; }
	double get_fovy() const { return m_fovy; }

private:
	vml::vec3 m_origin;
	vml::vec3 m_direction;
	double m_fovy;
};

#endif // _CAMERA_H_