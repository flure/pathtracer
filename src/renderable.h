#ifndef _RENDERABLE_H_
#define _RENDERABLE_H_

#include "ray.h"
#include "vml.h"
#include "material.h"
#include <memory>

class Renderable
{
public:
	Renderable() : m_material() {}
	Renderable(const Material& material) : m_material(material) {}
	Renderable(const Renderable& other) : m_material(other.m_material) {}

	Renderable& operator=(const Renderable& other)
	{
		m_material = other.m_material;
		return *this;
	}

	virtual ~Renderable() {};

	virtual bool intersect(const Ray& /*ray*/, double& /*dist*/) const { return false; }
	Material get_material() const { return m_material; }
	virtual vml::vec3 get_normal(const vml::vec3& /*pos*/) const { return vml::vec3(); }
	virtual vml::vec3 get_random_point() const { return vml::vec3(); }

protected:
	Material m_material;
};

class Sphere : public Renderable
{
public:
	Sphere()
		: Renderable(), m_center(), m_radius() {}
	Sphere(const vml::vec3& center, double radius, const Material& material)
		: Renderable(material), m_center(center), m_radius(radius) {}
	Sphere(const Sphere& other)
		: Renderable(other), m_center(other.m_center), m_radius(other.m_radius) {}
	virtual ~Sphere() {};

	Sphere& operator=(const Sphere& other)
	{
		Renderable::operator=(other);
		m_center = other.m_center;
		m_radius = other.m_radius;
		return *this;
	}

	vml::vec3 get_normal(const vml::vec3& position) const
	{
		return vml::normalize(position - m_center);
	}

	vml::vec3 get_center() const { return m_center; }

	virtual bool intersect(const Ray& ray, double& dist) const;
	virtual vml::vec3 get_random_point() const;

private:
	vml::vec3 m_center;
	double m_radius;
};

class Plane : public Renderable
{
public:
	Plane()
		: Renderable(), m_normal(), m_dist(0.0) {}
	Plane(const vml::vec3& normal, double dist, const Material& material)
		: Renderable(material), m_normal(normal), m_dist(dist) {}

	Plane(const Plane& other)
		: Renderable(other), m_normal(other.m_normal), m_dist(other.m_dist) {}

	Plane& operator=(const Plane& other)
	{
		Renderable::operator=(other);
		m_normal = other.m_normal;
		m_dist = other.m_dist;
		return *this;
	}

	virtual bool intersect(const Ray& ray, double& dist) const;
	virtual vml::vec3 get_normal(const vml::vec3& /*pos*/) const { return m_normal; }

private:
	vml::vec3 m_normal;
	double m_dist;
};

#endif // _RENDERABLE_H_