#ifndef _SCENE_H_
#define _SCENE_H_

#include <vector>
#include <memory>

#include "renderable.h"
#include "camera.h"
#include "ray.h"
#include "material.h"

class Scene
{
public:
	const Camera& get_camera() const { return m_camera; };

	void add_object(Renderable* obj)
	{
		m_objects.push_back(std::shared_ptr<Renderable>(obj));
	}

	const std::vector<std::shared_ptr<Renderable>>& get_objects() const { return m_objects; }

private:
	Camera m_camera;
	std::vector<std::shared_ptr<Renderable>> m_objects;
};

#endif // _SCENE_H_