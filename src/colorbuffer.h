#ifndef _COLORBUFFER_H_
#define _COLORBUFFER_H_

#include "color.h"
#include <memory>

class ColorBuffer
{
public:
	ColorBuffer()
		: m_width(0), m_height(0), m_sums(nullptr), m_nb_samples(0) {}
	ColorBuffer(int width, int height)
		: m_width(width), m_height(height), m_sums(new color::rgb[width * height]), m_nb_samples(0) {}
	ColorBuffer(const ColorBuffer& other)
		: m_width(other.m_width), m_height(other.m_height), m_sums(other.m_sums), m_nb_samples(other.m_nb_samples) {}

	ColorBuffer& operator=(const ColorBuffer& other)
	{
		m_width = other.m_width;
		m_height = other.m_height;
		m_sums = other.m_sums;
		m_nb_samples = other.m_nb_samples;
		return *this;
	}

	int get_width() const { return m_width; }
	int get_height() const { return m_height; }

	std::shared_ptr<color::rgb> get_sums() { return m_sums; }

	void average_and_copy(uint32_t* pixels) const
	{
		uint32_t* pix = pixels;
		color::rgb* sums = m_sums.get();

		for(int i = 0; i < m_width * m_height; i++)
		{
			*pix++ = as_32bpp(saturate(*sums / static_cast<double>(m_nb_samples)));
			sums++;
		}
	}

	void inc_samples() { m_nb_samples++; }
	void reset_samples() { m_nb_samples = 0; }

private:
	int m_width;
	int m_height;
	std::shared_ptr<color::rgb> m_sums;
	int m_nb_samples;
};

#endif // _COLORBUFFER_H_