#include "renderable.h"

#include "quadratic.h"
#include "utils.h"
#include <cmath>

#include <iostream>
#include <limits>
#include <cassert>

bool Sphere::intersect(const Ray& ray, double& dist) const
{
	assert(!ray.direction.isnan());
	assert(!ray.origin.isnan());

	vml::vec3 oc = ray.origin - m_center;
	assert(!oc.isnan());
	double a = vml::dot(ray.direction, ray.direction);
	double b = 2.0 * vml::dot(ray.direction, oc);
	double c = vml::dot(oc, oc) - m_radius * m_radius;

	Quadratic q(a, b, c);
	Quadratic::Solutions sol = q.solve();

	// no hit
	if(sol.nb_solutions == 0) return false;

	dist = sol.t0;
	if(sol.nb_solutions == 2)
	{
		dist = std::min(sol.t0, sol.t1);
		dist = dist < 0.0 ? std::max(sol.t0, sol.t1) : dist;
		if(dist < 0.0) return false;
		assert(!std::isnan(dist));
	}

	return true;
}

vml::vec3 Sphere::get_random_point() const
{
	return m_center;

	// double u = rand01<double>();
	// double v = rand01<double>();
	// //double r = rand11<double>() * m_radius;

	// double theta = 2.0 * M_PI * u;
	// double phi = std::acos(2.0 * v - 1.0);
	// double st = std::sin(theta);
	// vml::vec3 p(st * std::cos(phi), st * std::sin(phi), std::cos(theta));
	// return m_center + p * m_radius * 1.001;
}

bool Plane::intersect(const Ray& ray, double& dist) const
{
	assert(!ray.direction.isnan());
	assert(!ray.origin.isnan());

	double A = vml::dot(m_normal, ray.origin) + m_dist;
	double B = vml::dot(m_normal, ray.direction);

	if(std::abs(B) < 0.001) return false;
	if(std::abs(A) < 0.001) return false;

	dist = -A / B;
	if(dist < 0.0) return false;
	assert(!std::isnan(dist));

	return true;
}
