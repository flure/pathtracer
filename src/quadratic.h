#ifndef _QUADRATIC_H_
#define _QUADRATIC_H_

#include <cmath>

class Quadratic
{
public:
	struct Solutions
	{
		int nb_solutions;
		double t0;
		double t1;

		Solutions(int n = 0, double a = 0.0, double b = 0.0)
			: nb_solutions(n), t0(a), t1(b) {};
	};

	Quadratic(double a, double b, double c)
		: m_a(a), m_b(b), m_c(c) {};

	Quadratic(const Quadratic& other)
		: m_a(other.m_a), m_b(other.m_b), m_c(other.m_c) {};

	Solutions solve() const;

private:
	double m_a;
	double m_b;
	double m_c;
};

#endif // _QUADRATIC_H_