#include "utils.h"

#include <algorithm>
#include <cmath>
#include <cassert>

vml::vec3 random_vector_in_hemisphere(const vml::vec3& up)
{
	assert(!up.isnan());

	double phi = rand01<double>() * M_PI;
	double y = std::cos(phi);
	double x = std::sin(phi);
	double z = rand11<double>();


	// double y = rand11<double>();//2.0f * frand() - 1.0f;
	// double x = std::sqrt(1.0 - y * y) * std::cos(phi);
	// double z = std::sqrt(1.0 - y * y) * std::sin(phi);

	vml::vec3 v = vml::normalize(vml::vec3(x, y, z));
	double cs = vml::dot(v, up);
	if(cs <= 0.0) v = -v;
	return v;

}

vml::vec3 cosine_weighted_hemisphere(const vml::vec3& up)
{
	assert(!up.isnan());

	// double u1 = rand01<double>();
	// double u2 = rand01<double>();
	// double r = std::sqrt(u1);
 //    double theta = M_PI * u2;

 //    double x = r * std::cos(theta);
 //    double y = r * std::sin(theta);

 //    return vml::normalize(vml::vec3(x, y, std::sqrt(std::max(0.0, 1.0 - u1))));

	//vec3 d = jitter(nl, 2.*PI*rand(), sqrt(r2), sqrt(1. - r2));
	double r2 = rand01<double>();
	double phi = 2.0 * M_PI * rand01<double>();
	double sina = std::sqrt(r2);
	double cosa = std::sqrt(1.0 - r2);

	// vec3 w = normalize(d), u = normalize(cross(w.yzx, w)), v = cross(w, u);
	// return (u*cos(phi) + v*sin(phi)) * sina + w * cosa;

	vml::vec3 w = vml::normalize(up);
	vml::vec3 u = normalize(vml::cross(vml::orthogonal(w), w));
	vml::vec3 v = vml::cross(w, u);

	return (u*std::cos(phi) + v*std::sin(phi)) * sina + w * cosa;
}


float sfrand()
{
	static int s_rand_seed = 1337;
    s_rand_seed = 0x00269ec3 + s_rand_seed * 0x000343fd;
    int a = (s_rand_seed >> 16) & 32767;
    return( -1.0f + (2.0f / 32767.0f) * static_cast<float>(a) );
}