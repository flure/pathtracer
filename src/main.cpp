#include "vml.h"
#include "ray.h"
#include "material.h"
#include "renderable.h"
#include "utils.h"
#include "quadratic.h"
#include "camera.h"
#include "scene.h"
#include "pathtracer.h"
#include "window.h"
#include "colorbuffer.h"

#include <iostream>
#include <string>
#include <sstream>

#define WINDOW_WIDTH	640
#define WINDOW_HEIGHT	480
#define PIXELS_WIDTH	(WINDOW_WIDTH)
#define PIXELS_HEIGHT	(WINDOW_HEIGHT)

void update_title(Window& window, int num_sample)
{
	std::ostringstream oss;
	oss << "Path Tracer - " << num_sample << "spp";
	window.set_title(oss.str().c_str());
}

Scene create_scene()
{
	Scene scene;

	// //scene.add_object(new Sphere(vml::vec3(0.0, 32.499, -1.5), 30.0, Material(color::black, color::rgb(300.0, 300.0, 300.0))));
	// scene.add_object(new Sphere(vml::vec3(0.0, 2., -1.5), 0.3, Material(color::black, color::rgb(50.0, 50.0, 50.0))));
	// //scene.add_light(new PointLight(vml::vec3(1.0f, 2.3f, -1.5f), color::rgb(1.0f, 1.0f, 1.0f)));

	// scene.add_object(new Sphere(vml::vec3(0.0, 0.0, -2.0), 0.35, Material(color::rgb(1.0, 1.0, 0.0), color::black)));
	// scene.add_object(new Sphere(vml::vec3(-1.0, 0.5, -2.0), 0.5, Material(color::rgb(1.0, 0.0, 0.0), color::black)));
	// scene.add_object(new Sphere(vml::vec3(-1.0, -1.5, -2.0), 0.5, Material(color::rgb(0.0, 1.0, 0.0), color::black)));
	// scene.add_object(new Sphere(vml::vec3(2.0, -1.5, -2.0), 0.5, Material(color::rgb(0.0, 0.0, 1.0), color::black)));

	// scene.add_object(new Sphere(vml::vec3(0.0, 0.15, -1.5), 0.5, Material(color::rgb(0.0, 0.0, 1.0), color::black)));

	// scene.add_object(new Plane(vml::vec3(1.0, 0.0, 0.0), 2.5, Material(color::rgb(1.0, 0.5, 0.0), color::black)));	// L
	// scene.add_object(new Plane(vml::vec3(-1.0, 0.0, 0.0), 2.5, Material(color::rgb(1.0, 0.0, 0.5), color::black)));	// R
	// scene.add_object(new Plane(vml::vec3(0.0, 1.0, 0.0), 2.5, Material(color::rgb(0.0, 0.5, 1.0), color::black)));	// B
	// scene.add_object(new Plane(vml::vec3(0.0, -1.0, 0.0), 2.5, Material(color::rgb(0.5, 1.0, 1.0), color::black)));	// T
	// scene.add_object(new Plane(vml::vec3(0.0, 0.0, 1.0), 2.5, Material(color::rgb(1.0, 0.5, 1.0), color::black)));	// R
	// scene.add_object(new Plane(vml::vec3(0.0, 0.0, -1.0), 2.5, Material(color::rgb(1.0, 0.5, 1.0), color::black)));	// F


	scene.add_object(new Sphere(vml::vec3(0.0, 2.0, -1.5), 0.25, Material(color::white, 5.0, 0.0)));

	scene.add_object(new Sphere(vml::vec3(0.0, 0.0, -2.0), 0.35, Material(color::rgb(1.0, 1.0, 0.0))));
	scene.add_object(new Sphere(vml::vec3(-1.0, 0.5, -2.0), 1.0, Material(color::rgb(1.0, 0.0, 0.0), 0.0, 1.0, 1.0)));
	scene.add_object(new Sphere(vml::vec3(-1.0, -1.5, -2.0), 0.5, Material(color::rgb(0.0, 1.0, 0.0))));
	scene.add_object(new Sphere(vml::vec3(2.0, -1.5, -2.0), 1.0, Material(color::rgb(0.0, 0.0, 1.0), 0.0, 1.0, 1.0)));

	scene.add_object(new Sphere(vml::vec3(0.0, 0.15, -1.5), 0.5, Material(color::rgb(0.0, 0.0, 1.0))));

	scene.add_object(new Plane(vml::vec3(1.0, 0.0, 0.0), 2.5, Material(color::rgb(1.0, 0.5, 0.0))));	// L
	scene.add_object(new Plane(vml::vec3(-1.0, 0.0, 0.0), 2.5, Material(color::rgb(1.0, 0.0, 0.5))));	// R
	scene.add_object(new Plane(vml::vec3(0.0, 1.0, 0.0), 2.5, Material(color::rgb(0.0, 0.5, 1.0))));	// B
	scene.add_object(new Plane(vml::vec3(0.0, -1.0, 0.0), 2.5, Material(color::rgb(0.5, 1.0, 1.0))));	// T
	scene.add_object(new Plane(vml::vec3(0.0, 0.0, 1.0), 2.5, Material(color::rgb(1.0, 0.5, 1.0))));	// R
	scene.add_object(new Plane(vml::vec3(0.0, 0.0, -1.0), 2.5, Material(color::rgb(1.0, 0.5, 1.0))));	// F



	return scene;
}

int main(int /*argc*/, char** /*argv*/)
{
	PathTracer tracer(2);
	Scene scene = create_scene();

	Window win("pathtracer", WINDOW_WIDTH, WINDOW_HEIGHT);
	win.open(Window::State::Windowed);
	win.set_title("Test");

	ColorBuffer colors(PIXELS_WIDTH, PIXELS_HEIGHT);

	int num_sample = 1;
	while(!win.close_requested())
	{
		tracer.render_threaded(scene, colors, 4, 4);
		//tracer.render(scene, colors, 4, 4);

		uint32_t* pix;
		int pitch;
		win.get_pixels(&pix, &pitch);

		colors.average_and_copy(pix);

		win.release_pixels();
		update_title(win, num_sample);

		win.update();

		num_sample++;
		std::cout << "Sample #" << num_sample << std::endl;
	}
	win.close();

	return 0;
}