#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include "color.h"

#include "utils.h"
#include <cmath>

class Material
{
public:
	Material()
		: m_color(), m_emittance(0.0), m_diffuse(1.0), m_reflectance(0.0) {}

	Material(const color::rgb& col, double emit = 0.0, double diff = 1.0, double refl = 0.0)
		: m_color(col), m_emittance(emit), m_diffuse(diff), m_reflectance(refl) {}

	Material(const Material& other)
		: m_color(other.m_color), m_emittance(other.m_emittance), m_diffuse(other.m_diffuse),
		m_reflectance(other.m_reflectance) {}

	Material& operator=(const Material& other)
	{
		m_color = other.m_color;
		m_emittance = other.m_emittance;
		m_diffuse = other.m_diffuse;
		m_reflectance = other.m_reflectance;
		return *this;
	}

	virtual color::rgb get_diffuse(const vml::vec3& /*dir*/, const vml::vec3& nor, vml::vec3& out_dir) const
	{
		color::rgb brdf = m_color * m_diffuse / M_PI;
		double p = 1.0 / (2.0 * M_PI);

		out_dir = random_vector_in_hemisphere(nor);
		double ndotl = vml::dot(nor, out_dir);

		return brdf * ndotl / p;
	}

	virtual color::rgb get_emitted() const
	{
		return m_color * m_emittance;
	}

	virtual color::rgb get_reflected(const color::rgb& col) const
	{
		return m_reflectance * col;
	}

	virtual double get_reflectance() const { return m_reflectance; }

	virtual color::rgb get_color() const { return m_color; }

	virtual bool is_light() const { return m_emittance > 0.0; }

private:
	color::rgb m_color;
	double m_emittance;
	double m_diffuse;
	double m_reflectance;
};

#endif // _MATERIAL_H_