#ifndef _PATHTRACER_H_
#define _PATHTRACER_H_

#include "colorbuffer.h"
#include "scene.h"
#include "ray.h"
#include "vml.h"

#include <atomic>

class PathTracer
{
public:
	PathTracer(int nb_bounces = 1)
		: m_nb_bounces(nb_bounces), m_tile_counter(0), m_thread_counter(0) {};

	void render_threaded(const Scene& scene, ColorBuffer& colors, int num_tiles_x, int num_tiles_y);
	void render(const Scene& scene, ColorBuffer& colors, int num_tiles_x, int num_tiles_y);

private:

	color::rgb trace_scene(const Ray& ray, const Scene& scene, int num_bounce) const;
	color::rgb trace_scene2(const Ray& ray, const Scene& scene, int num_bounce) const;
	std::shared_ptr<Renderable> first_hit(const Ray& ray, const Scene& scene, double& t) const;
	void render_tile(const Scene& scene, ColorBuffer& colors, int tile_col, int tile_row, int tile_width, int tile_height);
	color::rgb lighting(const Scene& scene, const vml::vec3& pos, const vml::vec3& nor, const Material& mat) const;
	color::rgb reflect(const Ray& ray, const Scene& scene, const Material& mat, const color::rgb& col) const;

	int m_nb_bounces;
	std::atomic<int> m_tile_counter;
	std::atomic<int> m_thread_counter;

};

#endif // _PATHTRACER_H_