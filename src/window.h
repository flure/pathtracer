#ifndef _WINDOW_H_
#define _WINDOW_H_

#include "SDL.h"

#include <string>
#include <memory>

// test 123456

class Window
{
public:
	typedef enum
	{
		Windowed,
		Fullscreen
	} State;

	Window(const std::string& title = "", int w = 640, int h = 480);
	Window(const Window& other);
	~Window();

	Window& operator=(const Window& other);

	bool open(Window::State ws);
	void close();

	void set_title(const std::string& title);
	void update();
	bool close_requested() const;

	// pitch is the number of uint32_t per line (so byte number / 4)
	void get_pixels(uint32_t** pixels, int* pitch);
	void release_pixels();

	inline int get_width() const { return m_width; }
	inline int get_height() const { return m_height; }

private:
	SDL_Window* m_window;
	SDL_Renderer* m_renderer;
	SDL_Texture* m_texture;
	bool m_fullscreen;
	int m_width;
	int m_height;
	std::string m_title;
	State m_state;
	bool m_texture_updated;

	static bool s_sdl_inited;
};

#endif // _WINDOW_H_