#ifndef _VML_H_
#define _VML_H_

#include <cmath>
#include <cstdint>

namespace vml
{

//======================================================================================================================
//	2D vector
//======================================================================================================================
template<typename T>
struct vec2_
{
	T x, y;

	vec2_() : x(0), y(0) {}
	vec2_(T val) : x(val), y(val) {};
	vec2_(T vx, T vy) : x(vx), y(vy) {};
	vec2_(const vec2_<T> & other) : x(other.x), y(other.y) {};

	inline vec2_<T>& operator=(const vec2_<T>& other)
	{
		if(this != &other)
		{
			x = other.x;
			y = other.y;
		}
		return *this;
	}

	inline bool isnan() const { return std::isnan(x) || std::isnan(y); }

};

typedef vec2_<double> vec2;

template<typename T>
vec2_<T> operator-(const vec2_<T> vec)
{
	return vec2_<T>(-vec.x, -vec.y);
}

template<typename T>
vec2_<T>& operator+=(vec2_<T>& lhs, const vec2_<T>& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	return lhs;
}

template<typename T>
vec2_<T> operator+(const vec2_<T>& lhs, const vec2_<T>& rhs)
{
	return vec2_<T>(lhs.x + rhs.x, lhs.y + rhs.y);
}

template<typename T>
vec2_<T>& operator-=(vec2_<T>& lhs, const vec2_<T>& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	return lhs;
}

template<typename T>
vec2_<T> operator-(const vec2_<T>& lhs, const vec2_<T>& rhs)
{
	return vec2_<T>(lhs.x - rhs.x, lhs.y - rhs.y);
}

template<typename T>
vec2_<T>& operator*=(vec2_<T>& lhs, const vec2_<T>& rhs)
{
	lhs.x *= rhs.x;
	lhs.y *= rhs.y;
	return lhs;
}

template<typename T>
vec2_<T> operator*(const vec2_<T>& lhs, const vec2_<T>& rhs)
{
	return vec2_<T>(lhs.x * rhs.x, lhs.y * rhs.y);
}

template<typename T>
vec2_<T>& operator*=(vec2_<T>& lhs, T val)
{
	lhs.x *= val;
	lhs.y *= val;
	return lhs;
}

template<typename T>
vec2_<T> operator*(const vec2_<T>& lhs, T val)
{
	return vec2_<T>(lhs.x * val, lhs.y * val);
}

template<typename T>
vec2_<T> operator*(T val, const vec2_<T>& rhs)
{
	return vec2_<T>(val * rhs.x, val * rhs.y);
}

template<typename T>
vec2_<T>& operator/=(vec2_<T>& lhs, const vec2_<T>& rhs)
{
	lhs.x /= rhs.x;
	lhs.y /= rhs.y;
	return lhs;
}

template<typename T>
vec2_<T> operator/(const vec2_<T>& lhs, const vec2_<T>& rhs)
{
	return vec2_<T>(lhs.x / rhs.x, lhs.y / rhs.y);
}

template<typename T>
vec2_<T>& operator/=(vec2_<T>& lhs, T val)
{
	lhs.x /= val;
	lhs.y /= val;
	return lhs;
}

template<typename T>
float squared_length(const vec2_<T>& vec)
{
	return vec.x * vec.x + vec.y * vec.y;
}

template<typename T>
float length(const vec2_<T>& vec)
{
	return std::sqrt(vec.x * vec.x + vec.y * vec.y);
}

template<typename T>
vec2_<T> normalize(const vec2_<T>& vec)
{
	float l = length(vec);
	return vec / l;
}

template<typename T>
T dot(const vec2_<T>& lhs, const vec2_<T>& rhs)
{
	return lhs.x * rhs.x + lhs.y * rhs.y;
}


//======================================================================================================================
//	3D vector
//======================================================================================================================
template<typename T>
struct vec3_
{
	T x, y, z;

	vec3_() : x(), y(), z() {}
	vec3_(T val) : x(val), y(val), z(val) {};
	vec3_(T vx, T vy, T vz) : x(vx), y(vy), z(vz) {};
	vec3_(const vec3_<T>& other) : x(other.x), y(other.y), z(other.z) {};
	vec3_(const vec2_<T>& v, T vz) : x(v.x), y(v.y), z(vz) {}
	vec3_(T vx, const vec2_<T>& v) : x(vx), y(v.x), z(v.z) {}

	inline vec3_<T>& operator=(const vec3_<T>& other)
	{
		if(this != &other)
		{
			x = other.x;
			y = other.y;
			z = other.z;
		}
		return *this;
	}

	inline bool isnan() const { return std::isnan(x) || std::isnan(y) || std::isnan(z); }

};

typedef vec3_<double> vec3;



template<typename T>
vec3_<T> operator-(const vec3_<T>& vec)
{
	return vec3_<T>(-vec.x, -vec.y, -vec.z);
}

template<typename T>
vec3_<T>& operator+=(vec3_<T>& lhs, const vec3_<T>& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	lhs.z += rhs.z;
	return lhs;
}

template<typename T>
vec3_<T> operator+(const vec3_<T>& lhs, const vec3_<T>& rhs)
{
	return vec3_<T>(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}

template<typename T>
vec3_<T>& operator-=(vec3_<T>& lhs, const vec3_<T>& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	lhs.z -= rhs.z;
	return lhs;
}

template<typename T>
vec3_<T> operator-(const vec3_<T>& lhs, const vec3_<T>& rhs)
{
	return vec3_<T>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
}

template<typename T>
vec3_<T>& operator*=(vec3_<T>& lhs, const vec3_<T>& rhs)
{
	lhs.x *= rhs.x;
	lhs.y *= rhs.y;
	lhs.z *= rhs.z;
	return lhs;
}

template<typename T>
vec3_<T> operator*(const vec3_<T>& lhs, const vec3_<T>& rhs)
{
	return vec3_<T>(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z);
}

template<typename T>
vec3_<T>& operator*=(vec3_<T>& lhs, T val)
{
	lhs.x *= val;
	lhs.y *= val;
	lhs.z *= val;
	return lhs;
}

template<typename T>
vec3_<T> operator*(const vec3_<T>& lhs, T val)
{
	return vec3_<T>(lhs.x * val, lhs.y * val, lhs.z * val);
}

template<typename T>
vec3_<T> operator*(T val, const vec3_<T>& rhs)
{
	return vec3_<T>(val * rhs.x, val * rhs.y, val * rhs.z);
}

template<typename T>
vec3_<T>& operator/=(vec3_<T>& lhs, const vec3_<T>& rhs)
{
	lhs.x /= rhs.x;
	lhs.y /= rhs.y;
	lhs.z /= rhs.z;
	return lhs;
}

template<typename T>
vec3_<T>& operator/=(vec3_<T>& lhs, T val)
{
	lhs.x /= val;
	lhs.y /= val;
	lhs.z /= val;
	return lhs;
}

template<typename T>
vec3_<T> operator/(const vec3_<T>& lhs, const vec3_<T>& rhs)
{
	return vec3_<T>(lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z);
}

template<typename T>
vec3_<T> operator/(const vec3_<T>& lhs, T val)
{
	return vec3_<T>(lhs.x / val, lhs.y / val, lhs.z / val);
}


template<typename T>
float squared_length(const vec3_<T>& vec)
{
	return vec.x * vec.x + vec.y * vec.y + vec.z * vec.z;
}

template<typename T>
float length(const vec3_<T>& vec)
{
	return std::sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
}

template<typename T>
vec3_<T> normalize(const vec3_<T>& vec)
{
	T l = length(vec);
	return vec / l;
}

template<typename T>
T dot(const vec3_<T>& lhs, const vec3_<T>& rhs)
{
	return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}

template<typename T>
vec3_<T> cross(const vec3_<T>& a, const vec3_<T>& b)
{
	return vec3_<T>(a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

template<typename T>
vec3_<T> reflect(const vec3_<T>& d, const vec3_<T>& n)
{
	return n * T(2) - d;
}

template<typename T>
vec3_<T> orthogonal(const vec3_<T>& vec)
{
	return std::abs(vec.x) > std::abs(vec.z) ? vec3_<T>(-vec.y, vec.x, 0) : vec3_<T>(0, -vec.z, vec.y);
};

};


#endif // _VML_H_