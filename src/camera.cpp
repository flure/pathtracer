#include "camera.h"

#include <cmath>
#include <iostream>

Ray Camera::create_ray(const vml::vec2& v) const
{
	assert(!v.isnan());

	double fovy_rad = m_fovy * M_PI / 180.0;
	double z = -std::tan(fovy_rad * 0.5);
	vml::vec3 dir(v.x, v.y, z);

	assert(!m_origin.isnan());
	assert(!dir.isnan());

	return Ray(m_origin, vml::normalize(dir));
}