#ifndef _RAY_H_
#define _RAY_H_

#include "vml.h"
#include "material.h"
#include <memory>

#include <cassert>

struct Ray
{
	vml::vec3 origin;
	vml::vec3 direction;

	Ray()
		: origin(), direction(0.0, -1.0, 0.0) {};

	Ray(const vml::vec3& ori, const vml::vec3& dir)
		: origin(ori), direction(dir) {};

	Ray(const Ray& other)
		: origin(other.origin), direction(other.direction) {}

	Ray& operator=(const Ray& other)
	{
		origin = other.origin;
		direction = other.direction;
		return *this;
	}

	vml::vec3 get_point(double t) const
	{
		assert(!origin.isnan());
		assert(!direction.isnan());
		assert(!std::isnan(t));

		return origin + direction * t;
	};
};

#endif // _RAY_H_