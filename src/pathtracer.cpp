#include "pathtracer.h"

#include "vml.h"
#include "ray.h"
#include "utils.h"
#include "renderable.h"

#include <limits>
#include <thread>
#include <iostream>
#include <memory>
#include <algorithm>
#include <cassert>

std::shared_ptr<Renderable> PathTracer::first_hit(const Ray& ray, const Scene& scene, double& t) const
{
	assert(!ray.direction.isnan());
	assert(!ray.origin.isnan());

	double min_dist = std::numeric_limits<double>::max();

	std::shared_ptr<Renderable> ret;

	auto objs = scene.get_objects();
	double d;
	for(auto obj : objs)
	{
		if(obj->intersect(ray, d))
		{
			if(d < min_dist && d > 0.0)
			{
				ret = obj;
				min_dist = d;
			}
		}
	}

	t = min_dist;
	return ret;
}

color::rgb PathTracer::trace_scene(const Ray& ray, const Scene& scene, int num_bounce) const
{
	assert(!ray.direction.isnan());
	assert(!ray.origin.isnan());

	vml::vec3 dir=ray.direction;

	if(num_bounce > m_nb_bounces) return color::white;
	double dist;
	std::shared_ptr<Renderable> obj = first_hit(ray, scene, dist);
	if(!obj)
	{
		std::cout << "no hit ??? ####################################################\n";
		return color::black;
	}

	Material mat = obj->get_material();

	if(mat.is_light()) return mat.get_emitted();

	vml::vec3 pos = ray.get_point(dist);
	vml::vec3 nor = obj->get_normal(pos);
	assert(!nor.isnan());
	assert(!pos.isnan());

	vml::vec3 out_dir;


	color::rgb emitted = mat.get_emitted();
	color::rgb diffuse = mat.get_diffuse(ray.direction, nor, out_dir);
	assert(!out_dir.isnan());
	vml::vec3 new_pos = pos + 0.00001 * nor;
	assert(!new_pos.isnan());
	Ray incoming_ray(new_pos, out_dir);
	color::rgb incoming = trace_scene(incoming_ray, scene, num_bounce + 1);

	// vml::vec3 reflected = vml::reflect(ray.direction, nor);
	// assert(!reflected.isnan());
	// Ray refl_ray(new_pos, reflected);
	// color::rgb reflection(1.0);
	// if(mat.get_reflectance() > 0.0)
	// {
	// 	reflection = mat.get_reflected(trace_scene(refl_ray, scene, num_bounce + 1));
	// 	assert(!reflection.isnan());
	// }

	return  emitted + diffuse * incoming; // * reflection; //incoming;
}

color::rgb PathTracer::reflect(const Ray& ray, const Scene& scene, const Material& mat, const color::rgb& col) const
{
	double t;
	auto obj = first_hit(ray, scene, t);

	return mat.get_reflected(obj->get_material().get_color());
}

color::rgb PathTracer::lighting(const Scene& scene, const vml::vec3& pos, const vml::vec3& nor, const Material& mat) const
{
	color:: rgb ret;
	int nb_lights = 0;

	auto objs = scene.get_objects();
	for(auto obj : objs)
	{
		Material light_mat = obj->get_material();
		if(mat.is_light()) continue;

		vml::vec3 light_point = obj->get_random_point();
		vml::vec3 light_dir = light_point - pos;
		vml::vec3 new_pos = pos + 0.0001 * nor;
		Ray light_ray(new_pos, vml::normalize(light_dir));

		double d;
		auto o = first_hit(light_ray, scene, d);
		if(o != obj) continue;

		ret += light_mat.get_emitted() * std::max(0.0, vml::dot(light_dir, nor));
		nb_lights++;
	}

	return ret / static_cast<double>(nb_lights);
}

void PathTracer::render_tile(const Scene& scene, ColorBuffer& colors, int tile_col, int tile_row, int tile_width, int tile_height)
{
	int w = colors.get_width();
	int h = colors.get_height();
	int yoffs = tile_row * tile_height;
	int xoffs = tile_col * tile_width;

	double ratio = static_cast<double>(w) / static_cast<double>(h);
	double w2 = w / 2.0;
	double h2 = h / 2.0;
	Camera cam = scene.get_camera();

	for(int y = yoffs; y < yoffs + tile_height; y++)
	{
		color::rgb* sums = colors.get_sums().get() + y * w + xoffs;


		for(int x = xoffs; x < xoffs + tile_width; x++)
		{
			double vy = (-(static_cast<double>(y) + 0.5*rand11<double>() - h2) / h2) / ratio;
			double vx = (static_cast<double>(x) + 0.5*rand11<double>() - w2) / w2;

			Ray ray = cam.create_ray(vml::vec2(vx, vy));
			*sums++ += trace_scene(ray, scene, 0);
		}
	}

	m_tile_counter++;
}

void PathTracer::render_threaded(const Scene& scene, ColorBuffer& colors, int num_tiles_x, int num_tiles_y)
{
	int tw = colors.get_width() / num_tiles_x;
	int th = colors.get_height() / num_tiles_y;

	for(int y = 0; y < num_tiles_y; y++)
	{
		for(int x = 0; x < num_tiles_x; x++)
		{
			std::thread t(&PathTracer::render_tile, this, std::ref(scene), std::ref(colors), x, y, tw, th);
			t.detach();
		}
	}

	while(m_tile_counter < num_tiles_x * num_tiles_y) {}

	m_tile_counter = 0;

	colors.inc_samples();
}

void PathTracer::render(const Scene& scene, ColorBuffer& colors, int num_tiles_x, int num_tiles_y)
{
	int tw = colors.get_width() / num_tiles_x;
	int th = colors.get_height() / num_tiles_y;

	for(int y = 0; y < num_tiles_y; y++)
	{
		for(int x = 0; x < num_tiles_x; x++)
		{
			// std::thread t(&PathTracer::render_tile, this, std::ref(scene), std::ref(colors), x, y, tw, th);
			// t.detach();

			render_tile(scene, colors, x, y, tw, th);
		}
	}

	while(m_tile_counter < num_tiles_x * num_tiles_y) {}

	m_tile_counter = 0;

	colors.inc_samples();
}