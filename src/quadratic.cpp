#include "quadratic.h"

#include <cmath>

Quadratic::Solutions Quadratic::solve() const
{
	float delta = m_b * m_b - 4.0 * m_a * m_c;
	float a2 = 2.0 * m_a;
	if(delta < 0.0)
	{
		return Solutions(0);
	}
	else if(delta > 0)
	{
		float q = sqrt(delta);
		return Solutions(2, (-m_b + q) / a2, (-m_b - q) / a2);
	}
	else
	{
		float v = m_b / a2;
		return Solutions(1, v);
	}
}