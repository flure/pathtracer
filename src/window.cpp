#include "window.h"

#include <cstdint>

bool Window::s_sdl_inited = false;

Window::Window(const std::string& title, int w, int h)
		: m_window(nullptr), m_renderer(nullptr), m_texture(nullptr), m_fullscreen(false),
		m_width(w), m_height(h), m_title(title), m_state(State::Windowed), m_texture_updated(false)
{
	if(!Window::s_sdl_inited)
	{
		int init_result = SDL_Init(SDL_INIT_VIDEO);
		if(init_result == 0)
		{
			Window::s_sdl_inited = true;
		}
	}
}

Window::Window(const Window& other)
	: m_window(other.m_window), m_renderer(other.m_renderer), m_texture(other.m_texture),
	m_fullscreen(other.m_fullscreen), m_width(other.m_width), m_height(other.m_height), m_title(other.m_title),
	m_state(other.m_state), m_texture_updated(other.m_texture_updated)
{
}

Window::~Window()
{
	if(m_window)
	{
		close();
	}
	if(Window::s_sdl_inited)
	{
		SDL_Quit();
		Window::s_sdl_inited = false;
	}
}

Window& Window::operator=(const Window& other)
{
	m_window = other.m_window;
	m_renderer = other.m_renderer;
	m_texture = other.m_texture;
	m_fullscreen = other.m_fullscreen;
	m_width = other.m_width;
	m_height = other.m_height;
	m_title = other.m_title;
	m_state = other.m_state;
	return *this;
}

bool Window::open(Window::State ws)
{
	if(!Window::s_sdl_inited) return false;

	uint32_t flags = ws == Windowed ? 0 : SDL_WINDOW_FULLSCREEN;

	m_window = SDL_CreateWindow(m_title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_width, m_height, flags);

	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
	m_texture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, m_width, m_height);

	return m_window != nullptr;
}

void Window::close()
{
	if(!Window::s_sdl_inited) return;
	SDL_DestroyRenderer(m_renderer);
	SDL_DestroyWindow(m_window);
	m_renderer = nullptr;
	m_window = nullptr;
}

void Window::set_title(const std::string& title)
{
	m_title = title;
	if(!Window::s_sdl_inited) return;

	if(!m_window) return;
	SDL_SetWindowTitle(m_window, m_title.c_str());
}

bool Window::close_requested() const
{
	if(!Window::s_sdl_inited) return true;

	SDL_Event event;

	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_KEYDOWN:
			if(event.key.keysym.sym == SDLK_ESCAPE) return true;
			break;

		case SDL_WINDOWEVENT:
			if(event.window.event == SDL_WINDOWEVENT_CLOSE) return true;
		default:
			break;
		}
	}

	return false;
}

void Window::update()
{
	if(!Window::s_sdl_inited) return;

	if(m_texture_updated)
	{
		SDL_RenderCopy(m_renderer, m_texture, nullptr, nullptr);
		SDL_RenderPresent(m_renderer);
		m_texture_updated = false;
	}
}

void Window::get_pixels(uint32_t** pixels, int* pitch)
{
	if(!Window::s_sdl_inited) return;

	int p;
	SDL_LockTexture(m_texture, nullptr, (void**)pixels, &p);
	*pitch = p / 4;
}

void Window::release_pixels()
{
	if(!Window::s_sdl_inited) return;

	SDL_UnlockTexture(m_texture);
	m_texture_updated = true;
}