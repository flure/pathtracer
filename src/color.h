#ifndef _COLOR_H_
#define _COLOR_H_

#include "vml.h"

#include <cmath>
#include <cassert>


namespace color
{

template <typename T>
struct rgb_
{
	T r, g, b;

	rgb_()
		: r(T(0)), g(T(0)), b(T(0)) {}
	rgb_(const T& val)
		: r(T(val)), g(T(val)), b(T(val)) {}
	rgb_(const T& cr, const T& cg, const T& cb)
		: r(cr), g(cg), b(cb) {}
	rgb_(const rgb_<T>& other)
		: r(other.r), g(other.g), b(other.b) {}
	rgb_(uint32_t p)
		: r(((p & 0x00FF0000) >> 16) / 255.0f), g(((p & 0x0000FF00) >> 8) / 255.0f), b((p & 0x000000FF) / 255.0f) {}

	rgb_<T>& operator=(const rgb_<T>& other)
	{
		r = other.r;
		g = other.g;
		b = other.b;
		return *this;
	}

	bool isnan() const { return std::isnan(r) || std::isnan(g) || std::isnan(b); }

};

typedef rgb_<double> rgb;

const rgb black = rgb(0.0);
const rgb white = rgb(1.0);

template<typename T>
rgb_<T> operator-(const rgb_<T> color)
{
	return rgb_<T>(-color.r, -color.g, -color.b);
}

template<typename T>
rgb_<T>& operator+=(rgb_<T>& lhs, const rgb_<T>& rhs)
{
	lhs.r += rhs.r;
	lhs.g += rhs.g;
	lhs.b += rhs.b;
	return lhs;
}

template<typename T>
rgb_<T> operator+(const rgb_<T>& lhs, const rgb_<T>& rhs)
{
	return rgb_<T>(lhs.r + rhs.r, lhs.g + rhs.g, lhs.b + rhs.b);
}

template<typename T>
rgb_<T>& operator-=(rgb_<T>& lhs, const rgb_<T>& rhs)
{
	lhs.r -= rhs.r;
	lhs.g -= rhs.g;
	lhs.b -= rhs.b;
	return lhs;
}

template<typename T>
rgb_<T> operator-(const rgb_<T>& lhs, const rgb_<T>& rhs)
{
	return rgb_<T>(lhs.r - rhs.r, lhs.g - rhs.g, lhs.b - rhs.b);
}

template<typename T>
rgb_<T>& operator*=(rgb_<T>& lhs, const rgb_<T>& rhs)
{
	lhs.r *= rhs.r;
	lhs.g *= rhs.g;
	lhs.b *= rhs.b;
	return lhs;
}

template<typename T>
rgb_<T> operator*(const rgb_<T>& lhs, const rgb_<T>& rhs)
{
	return rgb_<T>(lhs.r * rhs.r, lhs.g * rhs.g, lhs.b * rhs.b);
}

template<typename T>
rgb_<T>& operator*=(rgb_<T>& lhs, T val)
{
	lhs.r *= val;
	lhs.g *= val;
	lhs.b *= val;
	return lhs;
}

template<typename T>
rgb_<T> operator*(const rgb_<T>& lhs, T val)
{
	return rgb_<T>(lhs.r * val, lhs.g * val, lhs.b * val);
}

template<typename T>
rgb_<T> operator*(T val, const rgb_<T>& rhs)
{
	return rhs * val;
}

template<typename T>
rgb_<T>& operator/=(rgb_<T>& lhs, const rgb_<T>& rhs)
{
	lhs.r /= rhs.r;
	lhs.g /= rhs.g;
	lhs.b /= rhs.b;
	return lhs;
}

template<typename T>
rgb_<T>& operator/=(rgb_<T>& lhs, T val)
{
	lhs.r /= val;
	lhs.g /= val;
	lhs.b /= val;
	return lhs;
}

template<typename T>
rgb_<T> operator/(const rgb_<T>& lhs, const rgb_<T>& rhs)
{
	return rgb_<T>(lhs.r / rhs.r, lhs.g / rhs.g, lhs.b / rhs.b);
}

template<typename T>
rgb_<T> operator/(const rgb_<T>& lhs, T val)
{
	return rgb_<T>(lhs.r / val, lhs.g / val, lhs.b / val);
}

template<typename T>
rgb_<T> clamp(const rgb_<T>& color, T min_val, T max_val)
{
	return rgb_<T>(color.r < min_val ? min_val : color.r > max_val ? max_val : color.r,
		color.g < min_val ? min_val : color.g > max_val ? max_val : color.g,
		color.b < min_val ? min_val : color.b > max_val ? max_val : color.b);
}

template<typename T>
rgb_<T> saturate(const rgb_<T>& color)
{
	return clamp<T>(color, T(0), T(1));
}

template<typename T>
bool is_black(const rgb_<T>& color)
{
	return (color.r > T(0)) && (color.g > T(0)) && (color.b > T(0));
}

template<typename T>
uint32_t as_32bpp(const rgb_<T>& color)
{
	return ((static_cast<uint8_t>(color.r * 255.0f)) << 16) +
		((static_cast<uint8_t>(color.g * 255.0f)) << 8) +
		(static_cast<uint8_t>(color.b * 255.0f));
}

}; // namespace color

#endif // _COLOR_H_