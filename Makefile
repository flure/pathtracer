SHELL=/bin/sh
CXX=clang++

BINDIR=build
$(shell mkdir -p $(BINDIR) >/dev/null)
DEPDIR=dep
$(shell mkdir -p $(DEPDIR) >/dev/null)
SRCDIR=src

SDLCXXFLAGS=`sdl2-config --cflags`
SDLLDFLAGS=`sdl2-config --libs`

OPTIM_FLAGS=
ifeq ($(DEBUG), 1)
	OPTIM_FLAGS=-DDEBUG -g
else
	OPTIM_FLAGS=-DNDEBUG -O3
endif

CXXSTD=c++1z
DEPFLAGS=-MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
CXXFLAGS=-std=$(CXXSTD) -Wall -Wextra -pedantic -Wshadow -Weffc++ $(SDLCXXFLAGS) $(OPTIM_FLAGS) $(DEPFLAGS)
LDFLAGS=$(SDLLDFLAGS) -lm -lpthread

POSTCOMPILE=@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@


SRC:=$(foreach sdir,$(SRCDIR),$(wildcard $(sdir)/*.cpp))
OBJ:=$(patsubst $(SRCDIR)/%.cpp,$(BINDIR)/%.o,$(SRC))

EXEC=pathtracer

.SUFFIXES:
.SUFFIXES: .cpp .o

all: $(EXEC)

$(EXEC): $(OBJ)
	@echo "Building $(EXEC) ..."
	@$(CXX) -o $@ $^ $(LDFLAGS)

#$(BINDIR)/%.o: $(SRCDIR)/%.cpp
$(BINDIR)/%.o: $(SRCDIR)/%.cpp $(DEPDIR)/%.d
	@echo "Compiling $< ..."
	@$(CXX) -o $@ -c $< $(CXXFLAGS)
	$(POSTCOMPILE)

.PHONY: clean mrproper

clean:
	@echo "Cleaning build dir ..."
	@rm -rf $(BINDIR)/*.o
	@echo "Cleaning dependencies dir ..."
	@rm -rf $(DEPDIR)/*.o

mrproper: clean
	@echo "Removing $(EXEC) ..."
	@rm -rf $(EXEC)

$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

include $(wildcard $(patsubst $(SRCDIR)/%.cpp,$(DEPDIR)/%.d,$(SRC)))
